<?php

/**
 * Sunovisio Extensions
 * http://ecommerce.sunovisio.com
 *
 * @extension   Quote PDF Printer
 * @type        Customer Support
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sunovisio
 * @package     Sunovisio_QuotePdfPrinter
 * @copyright   Copyright (c) 2012 Sunovisio (http://sunovisio.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
require_once(BP . DS . 'lib' . DS . 'Sunovisio' . DS . 'Tcpdf' . DS . 'tcpdf.php');

class Sunovisio_QuotePdfPrinter_Model_Quote extends Mage_Core_Model_Abstract {
//public $y;
//protected $_renderers = array();
// -- parameters to define in config --
    /*
     * margin-top
     * margin-left
     * margin-right
     * margin-bottom
     */

    CONST PAGE_WIDTH = 550;
    CONST COL_WIDTH_1 = '20%';
    CONST COL_WIDTH_2 = '30%';
    CONST COL_WIDTH_3 = '15%';
    CONST COL_WIDTH_4 = '15%';
    CONST COL_WIDTH_5 = '8%';
    CONST COL_WIDTH_6 = '12%';

    public function getPdf($quote) {
        $html = '';
        
// -- PDF Parameters --
        $pageOrientation = 'P';
        $unit = 'px';
        $pageFormat = 'A4';
        $marginTop = 20;
        $marginLeft = 0;
        $marginRight = 0;
        $marginBottom = 0;

        $rtl = 0;

        Mage::app()->getLocale()->emulate($quote->getStoreId());
        Mage::app()->setCurrentStore($quote->getStoreId());

        $pdf = new TCPDF($pageOrientation, $unit, $pageFormat, true, 'UTF-8', false);

        $baseFonts = array('times', 'courier', 'helvetica');

        if (Mage::getStoreConfig('quotepdfprinter/layout/font') == 'default' || !Mage::getStoreConfig('quotepdfprinter/layout/font')) {
            $fontname = $pdf->addTTFfont(BP . DS . 'lib' . DS . 'LinLibertineFont' . DS . 'LinLibertineC_Re-2.8.0.ttf', 'TrueTypeUnicode', '', 96);
        } else if (in_array(Mage::getStoreConfig('quotepdfprinter/layout/font'), $baseFonts)) {
            $fontname = Mage::getStoreConfig('quotepdfprinter/layout/font');
        } else {
            $fontname = $pdf->addTTFfont(BP . DS . 'media' . DS . 'fonts' . DS . Mage::getStoreConfig('quotepdfprinter/layout/font'), 'TrueTypeUnicode', '', 96);
        }

        $pdf->SetFont($fontname, '', 12);

        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);

        $pdf->AddPage();

        $pdf->setJPEGQuality(100);

        $imagePath = array(
            Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_SKIN),
            'frontend',
            !Mage::getStoreConfig('design/package/name') ? 'default' : Mage::getStoreConfig('design/package/name'),
            !Mage::getStoreConfig('design/theme/skin') ? 'default' : Mage::getStoreConfig('design/theme/skin'),
            Mage::getStoreConfig('design/header/logo_src')
        );

        $logoWithPath = join(DS, $imagePath);

        $address = Mage::getStoreConfig('sales/identity/address');
        $layout = Mage::getSingleton('core/layout');

        /** Logo and header */
        $html .= $layout->createBlock('core/template')->setTemplate('quotepdfprinter/quote/logo.phtml')
            ->setData('image', $logoWithPath)->setData('address', $address)->toHtml();

        /** @var $quote Mage_Sales_Model_Quote */
        $quote = Mage::registry('current_quote');

        $html .= $layout->createBlock('core/template')->setTemplate('quotepdfprinter/quote/header.phtml')->toHtml();

        $html .= $layout->createBlock('core/template')->setTemplate('quotepdfprinter/quote/body.phtml')->toHtml();

        $html .= $layout->createBlock('core/template')->setTemplate('quotepdfprinter/quote/totals.phtml')->toHtml();

        if (Mage::getStoreConfig('quotepdfprinter/frontend_parameters/terms_and_conditions')) {
            $html .= '<p style="text-align: center; ">'.nl2br(Mage::getStoreConfig('quotepdfprinter/frontend_parameters/terms_and_conditions')).'</p>';
        }

        if ($rtl) {
            $pdf->setRTL(true);
        }
        $pdf->writeHTML($html, true, false, true, false, '');
        return $pdf;
    }

}
